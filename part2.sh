#!/bin/bash

hostname="arch-cloud"
username="admin"

echo 'KEYMAP=dvorak' > /etc/vconsole.conf

set -e
bootmode="$1"
disk="$2"

ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
hwclock --systohc

LANG='en_US.UTF-8'
LC_ALL='en_US.UTF-8'
sed -i -e 's;#en_US.UTF-8 UTF-8;en_US.UTF-8 UTF-8;' /etc/locale.gen
locale-gen

pacman -Syu --noconfirm --needed vim emacs \
       openssh docker dhcpcd\
       git base-devel sudo\
       grub efibootmgr \
       #plasma kde-applications packagekit-qt5 



echo "$hostname" > /etc/hostname

echo '127.0.0.1    localhost' > /etc/hosts
echo '::1          localhost' >> /etc/hosts
echo '127.0.1.1    "$hostname".localdomain  "$hostname"' >> /etc/hosts

mkinitcpio -P


useradd -m "$username"

cd /home/"$username"/
mkdir Documents Downloads Desktop logs logs/stickets
chown -R "$username" /home/"$username"
echo "Enter password for $username "
while true; do if passwd "$username" ; then break; fi; done
echo "Enter password for root "
while true; do if passwd root ; then break; fi; done
groupadd sudo
usermod -a -G sudo,docker "$username"
printf ' %%sudo\tALL=(ALL) ALL' | EDITOR='tee -a' visudo
#passwd -l

if [[ "$bootmode" == 'UEFI' ]]; then
    grub_uefi.sh
else
    grub_legacy.sh "$disk"
fi

#TODO: automate firewall configurations, until then firewalld is disabled by default
sysctl -w net.ipv4.ip_forward=1
systemctl enable docker.service
systemctl enable sshd.service

exit
